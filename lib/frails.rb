# frozen_string_literal: true

require_relative "frails/version"

module Frails
  class Error < StandardError; end
  # Your code goes here...
end
